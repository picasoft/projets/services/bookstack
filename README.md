# Bookstack le nouveau turbowiki

## Lancer le service

Il suffit de cloner le dépôt et de lancer `docker compose up -d`.

## Premier lancement

Se connecter avec l'admin par défaut `admin@admin.com/password`. Dans `Settings>Default user role after registration` indiquer `Editor` sauvegarder. Et bien sûr changer le mot de passe de l'admin dans `Edit Profile>User password`.

Il va falloir modifier le fichier `/config/www/.env` à l'intérieur du conteneur et définir notamment, pour le mail (avec les bonnes informations, dans le pass) :
```
MAIL_FROM_NAME="BookStack"
MAIL_FROM=bookstack@picasoft.net

MAIL_HOST=mail.picasoft.net
MAIL_PORT=587
MAIL_USERNAME='example'
MAIL_PASSWORD='password'
MAIL_ENCRYPTION='tls'
```

Pour la synchronisation avec le LDAP (avec l'utilisateur `nss` dans le pass) :
```
AUTH_METHOD=ldap
LDAP_SERVER=ldaps://ldap.picasoft.net:636
LDAP_BASE_DN="ou=People,dc=picasoft,dc=net"
LDAP_DN='user'
LDAP_PASS='password'
LDAP_USER_FILTER='(&(uid=${user}))'
LDAP_VERSION=3
LDAP_ID_ATTRIBUTE=uid
LDAP_EMAIL_ATTRIBUTE=mail
LDAP_DISPLAY_NAME_ATTRIBUTE=cn
LDAP_THUMBNAIL_ATTRIBUTE=null
LDAP_START_TLS=false
```

On peut éventuellement ajouter `APP_DEBUG=true` en cas de problème ou `APP_LANG=fr` pour mettre l'application en français.

## Ajouter un·e administrateur·ice

On peut définir un utilisateur administrateur de l'instance en base en exécutant la requête suivante de l'invite de commande mysql :
```
UPDATE role_user SET role_id = 1 WHERE user_id = <id de l'utilisateur>;
```
